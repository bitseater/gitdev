/*
* Copyright (c) 2019 Carlos Suárez (https://gitlab.com/bitseater)
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public
* License as published by the Free Software Foundation; either
* version 3 of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* General Public License for more details.
*
* You should have received a copy of the GNU General Public
* License along with this program; If not, see <http://www.gnu.org/licenses/>.
*
* Authored by: Carlos Suárez <bitseater@gmail.com>
*/
namespace Gitdev.Widgets {

    public class Preferences : Gtk.Dialog {

        private Settings settings;

        public Preferences (Gtk.Window window) {
            settings = new Settings ("com.gitlab.bitseater.gitdev");

            this.title = _("Preferences");
            this.set_default_size (350, 200);
            this.transient_for = window;
            this.modal = true;
            this.resizable = false;
            this.border_width = 6;

            var theme_lab = new Gtk.Label (_("Dark theme"));
            theme_lab.halign = Gtk.Align.END;
            var theme = new Gtk.Switch ();
            theme.halign = Gtk.Align.START;
            if (settings.get_boolean ("dark")) {
                theme.active = true;
            } else {
                theme.active = false;
            }
            theme.notify["active"].connect (() => {
               if (theme.get_active ()) {
                   Gtk.Settings.get_default().set("gtk-application-prefer-dark-theme", true);
                   settings.set_boolean ("dark", true);
               } else {
                   Gtk.Settings.get_default().set("gtk-application-prefer-dark-theme", false);
                   settings.set_boolean ("dark", false);
               }
            });

            var layout = new Gtk.Grid ();
            layout.column_spacing = 12;
            layout.row_spacing = 18;
            layout.margin = 18;
            layout.attach (theme_lab, 0, 0, 1, 1);
            layout.attach (theme, 1, 0, 1, 1);

            var content = this.get_content_area () as Gtk.Box;
            content.add (layout);

            this.add_button (_("Close"), Gtk.ResponseType.CANCEL);
            this.response.connect (() => {
                this.destroy ();
            });

            this.show_all ();
        }
    }
}
