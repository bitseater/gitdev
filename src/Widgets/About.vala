/*
* Copyright (c) 2019 Carlos Suárez (https://gitlab.com/bitseater)
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public
* License as published by the Free Software Foundation; either
* version 3 of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* General Public License for more details.
*
* You should have received a copy of the GNU General Public
* License along with this program; If not, see <http://www.gnu.org/licenses/>.
*
* Authored by: Carlos Suárez <bitseater@gmail.com>
*/
namespace Gitdev.Widgets {

    public class About : Gtk.AboutDialog {

        public About (Gtk.Window window) {
            GLib.Object (transient_for: window, use_header_bar: 1);
            destroy_with_parent = true;
            modal = true;

            //Populate about dialog
            string[] author = Constants.AUTHOR;
            artists = author;
            authors = author;
            comments =  _("Gitlab client made with Vala and Gtk");
            copyright = "Copyright \xc2\xa9 2019 Carlos Suárez";
            documenters = author;
            license_type = Gtk.License.GPL_3_0;
            logo_icon_name = Constants.ICON_NAME;
            program_name = Constants.APP_NAME;
            translator_credits = Constants.TRANSLATORS;
            version = Constants.VERSION;
            website = Constants.WEB;
            website_label = _("GitDev website");
            wrap_license = true;

            //use_header_bar bug in elementaryOS
            foreach (var item in this.get_children ()) {
                if (item.name == "GtkHeaderBar") {
                    foreach (var child in (item as Gtk.Container).get_children ()) {
                        if (child.name == "GtkButton" || child.name == "GtkToggleButton") {
                            (item as Gtk.Container).remove (child);
                        }
                    }
                }
            }

            response.connect (() => {
                this.destroy ();
            });
        }
    }
}
