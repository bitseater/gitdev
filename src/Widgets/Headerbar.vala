/*
* Copyright (c) 2019 Carlos Suárez (https://gitlab.com/bitseater)
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public
* License as published by the Free Software Foundation; either
* version 3 of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* General Public License for more details.
*
* You should have received a copy of the GNU General Public
* License along with this program; If not, see <http://www.gnu.org/licenses/>.
*
* Authored by: Carlos Suárez <bitseater@gmail.com>
*/
namespace Gitdev.Widgets {

    public class Headerbar : Gtk.HeaderBar {

        public Headerbar (Gtk.Window window) {
            show_close_button = true;
            var app = (Gtk.Application) window.get_application ();

            var on_pref = new GLib.SimpleAction ("on_pref", null);
            on_pref.activate.connect (() => {
                var preferences = new Gitdev.Widgets.Preferences (window);
                preferences.run ();
            });
            app.add_action (on_pref);

            var on_quit = new GLib.SimpleAction ("on_quit", null);
            on_quit.activate.connect (() => {
                app.quit ();
            });
            app.add_action (on_quit);

            var on_about = new GLib.SimpleAction ("on_about", null);
            on_about.activate.connect (() => {
                var about = new Gitdev.Widgets.About (window);
                about.show ();
            });
            app.add_action (on_about);

            var app_button = new Gtk.Button.from_icon_name ("open-menu-symbolic", Gtk.IconSize.BUTTON);
            app_button.tooltip_text = _("Options");
            var menu = new GLib.Menu ();
            var section1 = new GLib.Menu ();
            var pref_item = new GLib.MenuItem (_("Preferences"), "app.on_pref");
            section1.append_item (pref_item);
            menu.append_section (null, section1);
            var section2 = new GLib.Menu ();
            var about_item = new GLib.MenuItem (_("About GitDev"), "app.on_about");
            var quit_item = new GLib.MenuItem (_("Quit"), "app.on_quit");
            section2.append_item (about_item);
            section2.append_item (quit_item);
            menu.append_section (null, section2);
            var popover = new Gtk.Popover.from_model (app_button, menu);
            app_button.clicked.connect (() => {
                popover.show_all ();
            });

            pack_end (app_button);
        }
    }
}
