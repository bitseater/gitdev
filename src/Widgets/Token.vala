/*
* Copyright (c) 2019 Carlos Suárez (https://gitlab.com/bitseater)
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public
* License as published by the Free Software Foundation; either
* version 3 of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* General Public License for more details.
*
* You should have received a copy of the GNU General Public
* License along with this program; If not, see <http://www.gnu.org/licenses/>.
*
* Authored by: Carlos Suárez <bitseater@gmail.com>
*/
namespace Gitdev.Widgets {

    public class Token : Gtk.Grid {

        private Settings settings;
        private string? token;
        private string? server;

        public Token (Gitdev.MainWindow window, Gtk.Box box, Gitdev.Widgets.Headerbar header) {
            halign = Gtk.Align.CENTER;
            valign = Gtk.Align.CENTER;
            margin = 12;
            row_spacing = 6;
            column_spacing = 6;
            column_homogeneous = true;

            var title_lab = new Gtk.Label (_("Log in your GitLab Account"));
            title_lab.get_style_context ().add_class ("H1");
            title_lab.halign = Gtk.Align.CENTER;
            title_lab.valign = Gtk.Align.CENTER;
            attach (title_lab, 1, 0, 4, 1);
            var image = new Gtk.Image.from_icon_name ("avatar-default-symbolic", Gtk.IconSize.DIALOG);
            image.halign = Gtk.Align.END;
            image.valign = Gtk.Align.CENTER;
            attach (image, 0, 3, 1, 2);
            var user_lab = new Gtk.Label (_("Username"));
            user_lab.halign = Gtk.Align.END;
            user_lab.valign = Gtk.Align.CENTER;
            attach (user_lab, 1, 3, 1, 1);
            var pass_lab = new Gtk.Label (_("Password"));
            pass_lab.halign = Gtk.Align.END;
            pass_lab.valign = Gtk.Align.CENTER;
            attach (pass_lab, 1, 4, 1, 1);
            var server_lab = new Gtk.Label (_("Server"));
            server_lab.halign = Gtk.Align.END;
            server_lab.valign = Gtk.Align.CENTER;
            attach (server_lab, 1, 5, 1, 1);
            var user_entry = new Gtk.Entry ();
            attach (user_entry, 2, 3, 3, 1);
            var pass_entry = new Gtk.Entry ();
            pass_entry.visibility = false;
            pass_entry.caps_lock_warning = true;
            attach (pass_entry, 2, 4, 3, 1);
            var server_combo = new Gtk.ComboBoxText ();
            server = "https://gitlab.com";
            server_combo.append_text ("https://gitlab.com");
            server_combo.append_text ("https://gitlab.gnome.org");
            server_combo.set_active  (0);
            server_combo.changed.connect (() => {
                server = server_combo.get_active_text ();
            });
            attach (server_combo, 2, 5, 3, 1);
            var log_button = new Gtk.Button.with_label (_("Log in"));
            log_button.get_style_context ().add_class (Gtk.STYLE_CLASS_SUGGESTED_ACTION);
            attach (log_button, 4, 6, 1, 1);
            log_button.clicked.connect (() => {
                if ((user_entry.get_text () == "") || (pass_entry.get_text () == "")) {
                    window.ticket.set_text (_("Username and password are mandatory"));
                    window.ticket.reveal_child = true;
                } else {
                    check_token (user_entry.get_text (), pass_entry.get_text (), window, box, header);
                }
            });
        }

        private void check_token (string user_str, string pass_str, Gitdev.MainWindow window, Gtk.Box box, Gitdev.Widgets.Headerbar header) {
            var uri = server + "/oauth/token";
            var str = "grant_type=password&username=" + user_str + "&password=" + pass_str;
            var session = new Soup.Session ();
            var message = new Soup.Message ("POST", uri);
            var buffer = Soup.MemoryUse.COPY;
            message.set_request("application/x-www-form-urlencoded; charset=utf-8", buffer, str.data);
            session.send_message (message);
            var response_msg = (string) message.response_body.flatten ().data;
            print (response_msg + "\n");
            if ("invalid_grant" in response_msg) {
                window.ticket.set_text (_("Login error. Try again, please"));
                window.ticket.reveal_child = true;
            } else {
                try {
                    var parser = new Json.Parser ();
                    parser.load_from_data (response_msg, -1);
                    var root_oa = parser.get_root ().get_object ();
                    token = root_oa.get_string_member ("access_token");
                    if (token != "") {
                        settings = new Settings ("com.gitlab.bitseater.gitdev");
                        settings.set_string ("token", token);
                        settings.set_string ("apiurl", server + "/api/v4");
                        box.remove (this);
                        var profile = new Gitdev.Views.MainView (header);
                        box.pack_end (profile, true, true, 0);
                        window.show_all ();
                    }
                } catch (Error e) {
                    debug (e.message);
                }
            }
        }
    }
}
