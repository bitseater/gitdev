/*
* Copyright (c) 2019 Carlos Suárez (https://gitlab.com/bitseater)
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public
* License as published by the Free Software Foundation; either
* version 3 of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* General Public License for more details.
*
* You should have received a copy of the GNU General Public
* License along with this program; If not, see <http://www.gnu.org/licenses/>.
*
* Authored by: Carlos Suárez <bitseater@gmail.com>
*/
namespace Gitdev {

    public class MainWindow : Gtk.Window {

        public GitdevApp app;
        private Settings settings;
        public Gitdev.Widgets.Ticket ticket;

        public MainWindow (GitdevApp app) {
            this.app = app;
            this.application = app;
            this.window_position = Gtk.WindowPosition.CENTER;

            //Define style
            var provider = new Gtk.CssProvider();
            provider.load_from_resource ("/com/gitlab/bitseater/gitdev/gitdev.css");
            Gtk.StyleContext.add_provider_for_screen (Gdk.Screen.get_default (), provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);

            //Geometry and theme
            settings = new Settings ("com.gitlab.bitseater.gitdev");
            if (settings.get_boolean ("dark")) {
                Gtk.Settings.get_default().set("gtk-application-prefer-dark-theme", true);
            } else {
                Gtk.Settings.get_default().set("gtk-application-prefer-dark-theme", false);
            }
            this.set_default_size(settings.get_int ("width"), settings.get_int ("height"));
            this.size_allocate.connect((a) => {
                int width, height;
                this.get_size (out width, out height);
                settings.set_int ("width", width);
                settings.set_int ("height", height);
            });

            //Compose mainwindow
            var header = new Gitdev.Widgets.Headerbar (this);
            this.set_titlebar (header);

            var overlay = new Gtk.Overlay ();
            var box = new Gtk.Box (Gtk.Orientation.VERTICAL, 0);
            overlay.add_overlay (box);
            ticket = new Gitdev.Widgets.Ticket ("");
            overlay.add_overlay (ticket);

            if (settings.get_string ("token") == "") {
                var token = new Gitdev.Widgets.Token (this, box, header);
                box.pack_end (token, true, true, 0);
            } else {
                var profile = new Gitdev.Views.MainView (header);
                box.pack_end (profile, true, true, 0);
            }

            this.add (overlay);
            this.show_all ();
        }
    }
}
