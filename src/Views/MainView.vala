/*
* Copyright (c) 2019 Carlos Suárez (https://gitlab.com/bitseater)
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public
* License as published by the Free Software Foundation; either
* version 3 of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* General Public License for more details.
*
* You should have received a copy of the GNU General Public
* License along with this program; If not, see <http://www.gnu.org/licenses/>.
*
* Authored by: Carlos Suárez <bitseater@gmail.com>
*/
namespace Gitdev.Views {

    public class MainView : Gtk.Paned {

        private Gitdev.Widgets.Headerbar header;
        public Gtk.Button back;

        public MainView (Gitdev.Widgets.Headerbar header) {
            this.header = header;
            orientation = Gtk.Orientation.HORIZONTAL;
            position = 250;

            //Create left side paned
            var leftbox = new Gtk.Box (Gtk.Orientation.VERTICAL, 0);
            leftbox.valign = Gtk.Align.FILL;
//            header.change_sensitive (true);

            //Retraying information and define elements
            var user = new Gitdev.Utils.GL_User ("user");
            header.title = user.username + " " + _("in gitlab.com");
//            header.show_search (_("Search in") + " " + _("Last News"));
//            var avatar = new Gtk.Image ();
//            try {
//                GLib.File imagen = GLib.File.new_for_uri(user.avatar_url);
//                InputStream input_stream = imagen.read();
//                var pixbuf = new Gdk.Pixbuf.from_stream_at_scale(input_stream, 64, 64, true);
//                avatar = new Gtk.Image.from_pixbuf(pixbuf);
//            } catch (Error e) {
//                stderr.printf ("Image not found");
//                avatar = new Gtk.Image.from_icon_name ("avatar-default", Gtk.IconSize.LARGE_TOOLBAR);
//            }
//            avatar.halign = Gtk.Align.START;
//            avatar.valign = Gtk.Align.START;
//            var name = new Gtk.Label (user.name);
//            name.halign = Gtk.Align.START;
//            name.valign = Gtk.Align.END;
//            name.get_style_context ().add_class ("H2");
//            var login = new Gtk.Label (user.login);
//            login.halign = Gtk.Align.START;
//            login.valign = Gtk.Align.CENTER;
//            login.get_style_context ().add_class ("H3");
//            var userbox = new Gtk.Box (Gtk.Orientation.VERTICAL, 0);
//            userbox.pack_start (name, false, false, 0);
//            userbox.pack_start (login, false, false, 0);
//            userbox.valign = Gtk.Align.CENTER;
//            var bio = new Gtk.Label (user.bio);
//            bio.max_width_chars = 25;
//            bio.ellipsize = Pango.EllipsizeMode.END;
//            bio.halign = Gtk.Align.START;
//            var location = new Gtk.Label (user.location);
//            location.halign = Gtk.Align.START;
//            var loc_img = new Gtk.Image.from_icon_name ("mark-location-symbolic", Gtk.IconSize.BUTTON);
//            var site = new Gtk.Box (Gtk.Orientation.HORIZONTAL, 0);
//            site.pack_start (loc_img, false, false, 0);
//            site.pack_start (location, true, true, 0);

//            //Pack content
//            var info = new Gtk.Grid ();
//            info.margin = 10;
//            info.row_spacing = 5;
//            info.column_spacing = 5;
//            info.valign = Gtk.Align.START;
//            info.attach (avatar, 0, 0, 1, 2);
//            info.attach (userbox, 1, 0, 1, 1);
//            info.attach (bio, 0, 2, 2, 1);
//            info.attach (site, 0, 3, 2, 1);

//            var url_event = Constants.API_URI + "users/" + user.login  + "/received_events?page=1&per_page=10";
//            var eventlist_first = new Gitdev.Lists.EventList (url_event, this);
//            eventlist_first.expand = true;

//            //create buttons
//            var switcher = new Gitdev.Widgets.Sidebar ();
//            switcher.new_row (null, _("Last News"), null);
//            switcher.new_row (null, _("Opened Issues"), null);
//            switcher.new_row (null, _("Notifications"), null);
//            switcher.new_row (null, _("Public Repositories"), user.pubrepos);
//            switcher.new_row (null, _("Public Gists"), user.pubgists);
//            switcher.new_row (null, _("Last Starred Repos"), null);
//            switcher.new_row (null, _("Last Starred Gists"), null);
//            switcher.new_row (null, _("Last Subscriptions"), null);
//            switcher.new_row (null, _("Followers"), user.followers);
//            switcher.new_row (null, _("Following"), user.following);

//            //create actions
//            switcher.row_activated.connect ((row) => {
//                switch (row.get_index ())
//                {
//                    case 0:
//                        var eventlist = new Gitdev.Lists.EventList (url_event, this);
//                        change_view (eventlist);
//                        header.show_search (_("Search in") + " " + _("Last news"));
//                        break;
//                    case 1:
//                        var url_issue = Constants.API_URI + "issues?page=1&per_page=10";
//                        var issuelist = new Gitdev.Lists.IssueList (url_issue, this);
//                        change_view (issuelist);
//                        header.show_search (_("Search in ") + _("Opened Issues"));
//                        break;
//                    case 2:
//                        var url_not = Constants.API_URI + "notifications?all=true&page=1&per_page=10";
//                        var notiflist = new Gitdev.Lists.NotifList (url_not, this);
//                        change_view (notiflist);
//                        header.show_search (_("Search in") + " " + _("Notifications"));
//                        break;
//                    case 3:
//                        var repos = new Gitdev.Lists.RepoList (Constants.API_URI + "user/repos?page=1&per_page=10", this);
//                        change_view (repos);
//                        header.show_search (_("Search in") + " " + _("Public Repositories"));
//                        break;
//                    case 4:
//                        var gists = new Gitdev.Lists.GistList (Constants.API_URI + "gists?page=1&per_page=10", this);
//                        change_view (gists);
//                        header.show_search (_("Search in") + " " + _("Public Gists"));
//                        break;
//                    case 5:
//                        var stars = new Gitdev.Lists.RepoList (Constants.API_URI + "user/starred?page=1&per_page=10", this);
//                        change_view (stars);
//                        header.show_search (_("Search in") + " " + _("Starred Repos"));
//                        break;
//                    case 6:
//                        var stargist = new Gitdev.Lists.GistList (Constants.API_URI + "gists/starred?page=1&per_page=10", this);
//                        change_view (stargist);
//                        header.show_search (_("Search in") + " " + _("Starred Gists"));
//                        break;
//                    case 7:
//                        var subs = new Gitdev.Lists.RepoList (Constants.API_URI + "user/subscriptions?page=1&per_page=10", this);
//                        change_view (subs);
//                        header.show_search (_("Search in") + " " + _("Subscriptions"));
//                        break;
//                    case 8:
//                        var fwers = new Gitdev.Lists.UserList (Constants.API_URI + "user/followers?page=1&per_page=10", this);
//                        change_view (fwers);
//                        header.show_search (_("Search in") + " " + _("Followers"));
//                        break;
//                    case 9:
//                        var fwing = new Gitdev.Lists.UserList (Constants.API_URI + "user/following?page=1&per_page=10", this);
//                        change_view (fwing);
//                        header.show_search (_("Search in") + " " + _("Following"));
//                        break;
//                }
//            });

//            //Pack action buttons
//            var action_box = new Gtk.ActionBar ();
//            action_box.get_style_context ().add_class (Gtk.STYLE_CLASS_INLINE_TOOLBAR);
//            var edit_button= new Gtk.Button.from_icon_name ("user-info-symbolic", Gtk.IconSize.BUTTON);
//            edit_button.tooltip_text = _("Edit profile");
//            edit_button.clicked.connect (() => {
////                var edit_user = new Gitdev.Widgets.EditUser ();
////                mainview.set_right_view (edit_user);
//            });
//            var repo_button = new Gtk.Button.from_icon_name ("folder-new-symbolic", Gtk.IconSize.BUTTON);
//            repo_button.tooltip_text = _("Create new repo");
//            repo_button.clicked.connect (() => {
//                //TODO create repo
//                var dialog = new Gtk.Dialog ();
//                dialog.show ();
//            });
//            var gist_button = new Gtk.Button.from_icon_name ("document-new-symbolic", Gtk.IconSize.BUTTON);
//            gist_button.tooltip_text = _("Create new gist");
//            gist_button.clicked.connect (() => {
//                //TODO create gist
//                var dialog = new Gtk.Dialog ();
//                dialog.show ();
//            });
//            action_box.pack_start (edit_button);
//            action_box.pack_start (repo_button);
//            action_box.pack_start (gist_button);

//            //Building profile window
//            leftbox.pack_start (info, false, false, 0);
//            leftbox.pack_start (switcher, true, true, 0);
//            leftbox.pack_end (action_box, false, false, 0);
//            this.pack1 (leftbox, true, false);
//            this.pack2 (eventlist_first, true, false);
        }

        public void change_view (Gtk.Widget widget) {
            var child = this.get_child2 ();
            this.remove (child);
            this.pack2 (widget, true, false);
            widget.show_all ();
        }

        public void add_back (Gtk.Widget widget) {
            back = new Gtk.Button.with_label ("<< " + _("Go back"));
            header.pack_start (back);
            back.clicked.connect (() => {
                change_view (widget);
                back.destroy ();
            });
            back.show_all ();
        }

        public void remove_back () {
            if (back != null) {
                back.destroy ();
            }
        }
    }
}
