/*
* Copyright (c) 2019 Carlos Suárez (https://gitlab.com/bitseater)
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public
* License as published by the Free Software Foundation; either
* version 3 of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* General Public License for more details.
*
* You should have received a copy of the GNU General Public
* License along with this program; If not, see <http://www.gnu.org/licenses/>.
*
* Authored by: Carlos Suárez <bitseater@gmail.com>
*/
namespace  Gitdev.Utils {

    public class GL_User : Object {

        private Json.Object root;

        public string username;
        public string email;
        public string name;
        public string state;
        public string avatar_url;
        public string web_url;
        public string created_at;
        public string bio;
        public string location;
        public string public_email;
        public string skype;
        public string linkedin;
        public string twitter;
        public string website_url;
        public string organization;
        public string last_sign_in_at;
        public string confirmed_at;
        public string last_activity_on;
        public string projects_limit;
        public string current_sign_in_at;
        public bool can_create_group;
        public bool can_create_project;
        public bool two_factor_enabled;
        public bool external;
        public bool private_profile;

        public GL_User (string url) {
            var settings = new Settings ("com.gitlab.bitseater.gitdev");
            string token = settings.get_string ("token");
            var uri = settings.get_string ("apiurl") + "/" + url;
            print (uri);
            string str = "";
            var session = new Soup.Session ();
            var message = new Soup.Message ("GET", uri);
            var buffer = Soup.MemoryUse.COPY;
            message.request_headers.append ("User-Agent", "gitdev");
            message.set_request("application/json; charset=utf-8", buffer, str.data);
            message.request_headers.append ("Authorization", "Bearer " + token);
            session.send_message (message);
            var response_msg = (string) message.response_body.flatten ().data;

            try {
                var parser = new Json.Parser ();
                parser.load_from_data (response_msg, -1);
                root = parser.get_root ().get_object ();
                username = check_member ("username");
                email = check_member ("email");
                name = check_member ("name");
                state = check_member ("state");
                avatar_url = check_member ("avatar_url");
                web_url = check_member ("web_url");
                created_at = check_member ("created_at");
                bio = check_member ("bio");
                location = check_member ("location");
                public_email = check_member ("public_email");
                skype = check_member ("skype");
                linkedin = check_member ("linkedin");
                twitter =  check_member ("twitter");
                website_url =  check_member ("website_url");
                organization =  check_member ("organization");
                last_sign_in_at =  check_member ("last_sign_in_at");
                confirmed_at =  check_member ("confirmed_at");
                last_activity_on =  check_member ("last_activity_on");
                projects_limit = root.get_int_member ("projects_limit").to_string ();
                current_sign_in_at =  check_member ("current_sign_in_at");
                can_create_group =  root.get_boolean_member ("can_create_group");
                can_create_project =  root.get_boolean_member ("can_create_project");
                two_factor_enabled =  root.get_boolean_member ("two_factor_enabled");
                external =  root.get_boolean_member ("external");
                print (response_msg);
            } catch (Error e) {
                debug (e.message);
            }
        }

        private string check_member (string member) {
            string result = "";
            unowned Json.Node item = this.root.get_member (member);
            if (item.get_string () != null) {
                    result = item.get_string ();
            }
            return result;
        }
    }
}
